const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, changeFile, delFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

// Other endpoints - put, delete.

router.put('/:filename', changeFile);

router.delete('/:filename', delFile);

module.exports = {
  filesRouter: router
};
