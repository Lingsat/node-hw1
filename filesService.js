// requires...
const fs = require("fs");
const fsPromises = require("fs").promises;
const path = require("path");
// constants...

const validExtensions = ["txt", "log", "json", "yaml", "xml", "js"];

function createFile(req, res, next) {
  const fileExt = path.extname(req.body.filename).slice(1);

  if (!req.body.content) {
    res.status(400).send({ message: "Please specify 'content' parameter" });
  } else if (validExtensions.includes(fileExt)) {
    fs.writeFile(
      path.join(__dirname, "files", req.body.filename),
      req.body.content,
      function () {
      }
    );
    res.status(200).send({ message: "File created successfully" });
  } else {
    res
      .status(400)
      .send({ message: "Wrong file extension" });
  }
}

function getFiles(req, res, next) {
  fs.readdir(path.join(__dirname, "files"), (error, files) => {
    if (files.length) {
      res.status(200).send({
        message: "Success",
        files: files,
      });
    } else {
      res.status(400).send({ message: "Client error" });
    }
  });
}

const getFile = (req, res, next) => {
  fs.readdir(path.join(__dirname, "files"), (error, files) => {
    if (files.includes(req.params.filename)) {
      let fileUploadedDate;
      fs.stat(
        path.join(__dirname, "files", req.params.filename),
        (err, stats) => {
          fileUploadedDate = stats.ctime;
        }
      );
      const fileName = path.basename(req.params.filename);
      const fileExtension = path.extname(req.params.filename).slice(1);

      fs.readFile(
        path.join(__dirname, "files", req.params.filename),
        (error, data) => {
          res.status(200).send({
            message: "Success",
            filename: fileName,
            content: data.toString(),
            extension: fileExtension,
            uploadedDate: fileUploadedDate,
          });
        }
      );
    } else {
      res.status(400).send({
        message: `No file with '${req.params.filename}' filename found`,
      });
    }
  });
};

// Other functions - editFile, deleteFile

function changeFile(req, res) {
  fs.readdir(path.join(__dirname, "files"), (error, files) => {
    if (files.includes(req.params.filename)) {
      fs.writeFile(
        path.join(__dirname, "files", req.params.filename),
        req.body.content,
        function () {}
      );
      res.status(200).send({ message: "File changed successfully" });
    } else {
      res.status(400).send({ message: "File not found" });
    }
  });
}

function delFile(req, res) {
  fs.readdir(path.join(__dirname, "files"), (error, files) => {
    if (files.includes(req.params.filename)) {
      fs.unlinkSync(path.join(__dirname, "files", req.params.filename));
      res.status(200).send({ message: "File deleted successfully" });
    } else {
      res.status(400).send({ message: "File not found" });
    }
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  changeFile,
  delFile,
};
